# Hc2i_MGA_mobile_app

Gard Shifting manage app 
<h3>Admin Space</h3>
<ul>
<li>Create user system including user roles, permissions, groups and authentication<b>(30h)</b></li>
<li>Create a shifting system that include 
    <ul>
     <li>Manage users shifts by user/zone/group and show users location <b>(26h)</b></li>
     <li>Report and statistics by date/user/zone/group<b>(13h)</b></li>
     <li>Manage absences (approve/reject or replace)<b>(15h)</b></li>
    </ul>
</li>
<li>Manage User (crud) check shift history/absence/locations/zones<b>(17h)</b></li>
<li>Manage zones and work areas (crud/location), user can be linked to many zones<b>(10h)</b></li>
<li>Console and receive notifications<b>(22h)</b></li>
</ul>
<h3>Users Space</h3>
<ul>
<li>Create user profile / authentication<b>(15h)</b></li>
<li>Create a shifting system that include 
    <ul>
     <li>Time counter<b>(11h)</b></li> 
     <li>Start/stop shift (location validation)<b>(10h)</b></li>
     <li>Start/stop break (location validation)<b>(10h)</b></li>
     <li>Owned shift details<b>(18h)</b></li>
     <li>Report and statistics by date.<b>(30h)</b></li>
     <li>User can request absence by date (from => to) with sending notification to admins to approve/reject or replace<b>(21h)</b></li>
    </ul>
</li>
<li>Console and receive notifications using socket <b>(28h)</b></li>
</ul>


<h3>The time estimation is between 42 and 50 days<h3/>

<li>React with <small><b>(Hc2i_MGA_app)</b></small> API app <b></b></li>


<small style="float: right"> <a href="https://hc2i.tech/"> &#169;Hc2i Maroc 	&#174;</a></small>
